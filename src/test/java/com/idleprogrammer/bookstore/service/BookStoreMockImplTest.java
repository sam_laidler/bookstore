package com.idleprogrammer.bookstore.service;

import java.util.List;

import org.junit.Test;

import com.idleprogrammer.bookstore.domain.Book;

public class BookStoreMockImplTest {

	@Test(expected=BookTooExpensiveException.class)
	public void testAddNewBookWhenBookTooExpensive() throws BookTooExpensiveException {
		BookStoreMockImpl bookStore = new BookStoreMockImpl();
		bookStore.addNewBook(new Book(1, 1001, "Pricey book"));
		List<Book> bookList = bookStore.getAllBooks();
		assert(bookList.isEmpty());
	}

	@Test
	public void testAddNewBookWhenBookNotTooExpensive() throws BookTooExpensiveException {
		BookStoreMockImpl bookStore = new BookStoreMockImpl();
		bookStore.addNewBook(new Book(2, 1000, "Less pricey book"));
		List<Book> bookList = bookStore.getAllBooks();
		assert(bookList.isEmpty() == false);
	}
}
