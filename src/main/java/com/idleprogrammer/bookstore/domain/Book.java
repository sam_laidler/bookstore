package com.idleprogrammer.bookstore.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {
	@Id
	private int isbn;
	private double price;
	private String title;
	
	public Book() {}

	public Book(int isbn, double price, String title) {
		super();
		this.isbn = isbn;
		this.price = price;
		this.title = title;
	}

	@Override
	public String toString() {
		return isbn + " " + price + " " + title;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
