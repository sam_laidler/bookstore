package com.idleprogrammer.bookstore.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.idleprogrammer.bookstore.domain.Book;
import com.idleprogrammer.bookstore.service.BookStoreInterface;
import com.idleprogrammer.bookstore.service.BookTooExpensiveException;

public class ClientDaoTesterQuick {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext container = new ClassPathXmlApplicationContext("beanConfig.xml");
		BookStoreInterface bookStore = (BookStoreInterface) container.getBean("BookStore");
		Book newBook = new Book(1234567, 11.99, "Sam's book");
		try {
			System.out.println("Adding book...");
			bookStore.addNewBook(newBook);
		} catch (BookTooExpensiveException e) {
			System.out.println("That book is too expensive");
		}
		System.out.println("Book added");

		System.out.println("Displaying all books...");
		for (Book book : bookStore.getAllBooks()) {
			System.out.println(book);
		}
		System.out.println("All books displayed");
		
		container.close();
	}

}
