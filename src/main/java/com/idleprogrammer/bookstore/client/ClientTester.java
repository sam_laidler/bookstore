package com.idleprogrammer.bookstore.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.idleprogrammer.bookstore.domain.Book;
import com.idleprogrammer.bookstore.service.BookStoreInterface;
import com.idleprogrammer.bookstore.service.BookTooExpensiveException;

public class ClientTester {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext container = new ClassPathXmlApplicationContext("beanConfig.xml");
		BookStoreInterface bookStore = (BookStoreInterface) container.getBean("MockBookStore");
		
		Book book1 = new Book(1, 12.34, "Lassy: the auto-biography");
		Book book2 = new Book(2, 5.99, "How to be an idle programmer");
		Book book3 = new Book(3, 9999.99, "A book about Sam Laidler (and why he's just so great)");
		Book book4 = new Book(4, 99.99, "Practical uses for dirt");
		
		List<Book> books = new ArrayList();
		books.add(book1);
		books.add(book2);
		books.add(book3);
		books.add(book4);
		
		for (Book book : books) {
			try {
				bookStore.addNewBook(book);
			} catch (BookTooExpensiveException e) {
				e.printStackTrace();
			}
		}

		bookStore.removeBook(book2.getIsbn());

		// only books 1 & 4 should make it...
		for (Book book : bookStore.getAllBooks()) {
			System.out.println(book);
		}
		container.close();
	}
}
