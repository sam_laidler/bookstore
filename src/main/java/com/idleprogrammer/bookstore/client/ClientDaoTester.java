package com.idleprogrammer.bookstore.client;

import java.util.Scanner;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.idleprogrammer.bookstore.domain.Book;
import com.idleprogrammer.bookstore.service.BookStoreInterface;
import com.idleprogrammer.bookstore.service.BookTooExpensiveException;

public class ClientDaoTester {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext container = new ClassPathXmlApplicationContext("beanConfig.xml");
		BookStoreInterface bookStore = (BookStoreInterface) container.getBean("BookStore");
		container.close();
		
		System.out.println("Choose an option:");
		System.out.println("C: Create a book");
		System.out.println("R: Retrieve all books");
		System.out.println("U: Update a book");
		System.out.println("D: Delete a book");
		System.out.println("X: Exit");
		
		Scanner scanner = new Scanner(System.in);
		
		char inputChar;
		int isbn;
		double price;
		String title;
		
		do {

			isbn = 0;
			price = 0.0;
			title = null;
			
			String inputLine = scanner.nextLine();
			inputChar = inputLine.charAt(0);
		
			switch (inputChar) {
			case 'c':
			case 'C':
				System.out.println("Enter ISBN");
				isbn = Integer.parseInt(scanner.nextLine());
				System.out.println("Enter Price");
				price = Double.parseDouble(scanner.nextLine());
				System.out.println("Enter Title");
				title = scanner.nextLine();
				Book newBook = new Book(isbn, price, title);
				try {
					bookStore.addNewBook(newBook);
				} catch (BookTooExpensiveException e) {
					System.out.println("That book is too expensive");
				}
				break;
				
			case 'r':
			case 'R':
				for (Book book : bookStore.getAllBooks()) {
					System.out.println(book);
				}
				break;
				
			case 'u':
			case 'U':
				System.out.println("Enter ISBN");
				isbn = Integer.parseInt(scanner.nextLine());
				Book bookToUpdate = bookStore.getBook(isbn);
				if (bookToUpdate == null) {
					System.out.println("Cannot find that book");
				} else {
					System.out.println("Re-enter Price");
					price = Double.parseDouble(scanner.nextLine());
					System.out.println("Re-enter Title");
					title = scanner.nextLine();
					bookToUpdate.setPrice(price);
					bookToUpdate.setTitle(title);
					
					bookStore.changeBook(bookToUpdate);
				}
				break;
				
			case 'd':
			case 'D':
				System.out.println("Enter ISBN of book to be deleted");
				isbn = Integer.parseInt(scanner.nextLine());
				bookStore.removeBook(isbn);
				break;
				
			case 'x':
			case 'X':
				System.out.println("Bye!");
				break;
				
			default:
				System.out.println("Invalid command");
			}

		} while(inputChar != 'X' && inputChar != 'x');
		
		scanner.close();
	}
}
