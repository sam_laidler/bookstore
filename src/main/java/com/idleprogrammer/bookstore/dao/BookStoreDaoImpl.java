package com.idleprogrammer.bookstore.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.query.Query;

import com.idleprogrammer.bookstore.domain.Book;

public class BookStoreDaoImpl implements BookStoreDao {

	public BookStoreDaoImpl() {
	}

	public void create(Book book) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreDB");
		EntityManager em = emf.createEntityManager();
		EntityTransaction trn = em.getTransaction();
		trn.begin();

		em.persist(book);

		trn.commit();
		em.close();
	}

	public List<Book> readAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreDB");
		EntityManager em = emf.createEntityManager();
		EntityTransaction trn = em.getTransaction();
		trn.begin();

		@SuppressWarnings("rawtypes")
		Query query = (Query) em.createQuery("select book from Book book");
		@SuppressWarnings("unchecked")
		List<Book> books = (List<Book>) query.getResultList();
		
		trn.commit();
		em.close();
		return books;
	}

	public void update(Book book) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreDB");
		EntityManager em = emf.createEntityManager();
		EntityTransaction trn = em.getTransaction();
		trn.begin();

		Book originalBook = em.find(Book.class, book.getIsbn());

		if (originalBook != null) {
			originalBook.setPrice(book.getPrice());
			originalBook.setTitle(book.getTitle());
		}

		trn.commit();
		em.close();
	}

	public void delete(int isbn) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreDB");
		EntityManager em = emf.createEntityManager();
		EntityTransaction trn = em.getTransaction();
		trn.begin();

		Book book = em.find(Book.class, isbn);
		em.remove(book);

		trn.commit();
		em.close();
	}

	@Override
	public Book read(int isbn) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreDB");
		EntityManager em = emf.createEntityManager();
		EntityTransaction trn = em.getTransaction();
		trn.begin();

		Book book = em.find(Book.class, isbn);

		trn.commit();
		em.close();

		return book;
	}

}
