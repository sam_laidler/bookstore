package com.idleprogrammer.bookstore.dao;

import java.util.List;

import com.idleprogrammer.bookstore.domain.Book;

public interface BookStoreDao {
	public void create(Book book);
	public List<Book> readAll();
	public void update(Book book);
	public void delete(int isbn);
	public Book read(int isbn);
}
