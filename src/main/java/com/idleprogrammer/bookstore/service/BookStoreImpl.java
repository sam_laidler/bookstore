package com.idleprogrammer.bookstore.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.idleprogrammer.bookstore.dao.BookStoreDao;
import com.idleprogrammer.bookstore.domain.Book;

public class BookStoreImpl implements BookStoreInterface {
	BookStoreDao bookStoreDao;
	double maximumBookPrice;
	
	public BookStoreImpl(BookStoreDao bookStoreDao) {
		this();
		this.bookStoreDao = bookStoreDao;
	}
	
	public BookStoreImpl() {
		Properties props;
		props  = new Properties();
		try {
			InputStream in = this.getClass().getResourceAsStream("BookStore.properties");
			props.load(in);
			maximumBookPrice = Double.parseDouble(props.getProperty("maximumBookPrice"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addNewBook(Book book) throws BookTooExpensiveException {
		if (book.getPrice() > maximumBookPrice) {
			throw new BookTooExpensiveException();
		}
		bookStoreDao.create(book);
	}

	@Override
	public List<Book> getAllBooks() {
		return bookStoreDao.readAll();
	}

	@Override
	public Book getBook(int isbn) {
		return bookStoreDao.read(isbn);
	}

	@Override
	public void removeBook(int isbn) {
		bookStoreDao.delete(isbn);
	}

	@Override
	public void changeBook(Book book) {
		bookStoreDao.update(book);
	}
}
