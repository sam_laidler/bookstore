package com.idleprogrammer.bookstore.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.idleprogrammer.bookstore.domain.Book;

public class BookStoreMockImpl implements BookStoreInterface {
	List<Book> books;
	double maximumBookPrice;
	
	public BookStoreMockImpl() {
		books = new ArrayList<Book>();
		Properties props;
		props  = new Properties();
		try {
			InputStream in = this.getClass().getResourceAsStream("BookStore.properties");
			props.load(in);
			maximumBookPrice = Double.parseDouble(props.getProperty("maximumBookPrice"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Let's imagine a business rule: we're not allowed to keep books that are too expensive
	 * I guess it's a bit weird to work business logic into a mock implementation.
	 * I should have waited to create a production implementation but I was just too impatient.
	 * @throws BookTooExpensiveException 
	 */
	@Override
	public void addNewBook(Book book) throws BookTooExpensiveException {
		
		if (book.getPrice() > maximumBookPrice) {
			throw new BookTooExpensiveException();
		}
		
		books.add(book);
	}

	@Override
	public Book getBook(int isbn) {
		for (Book book : books) {
			if (book.getIsbn() == isbn) {
				return book;
			}
		}
		return null;
	}

	@Override
	public List<Book> getAllBooks() {
		return books;
	}

	@Override
	public void removeBook(int isbn) {
		books.remove(getBook(isbn));
	}

	@Override
	public void changeBook(Book book) {
		Book originalBook = getBook(book.getIsbn());
		
		if (originalBook == null) {
			return;
		}
		
		originalBook.setPrice(book.getPrice());
		originalBook.setTitle(book.getTitle());
	}
}
