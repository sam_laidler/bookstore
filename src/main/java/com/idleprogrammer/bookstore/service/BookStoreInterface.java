package com.idleprogrammer.bookstore.service;

import java.util.List;

import com.idleprogrammer.bookstore.domain.Book;

public interface BookStoreInterface {
	public void addNewBook(Book book) throws BookTooExpensiveException;
	public List<Book> getAllBooks();
	public Book getBook(int isbn);
	public void removeBook(int isbn);
	public void changeBook(Book book);
}
